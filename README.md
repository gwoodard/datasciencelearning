# **README** #

This repo contains folders of modules for an online Machine Learning course about Python and R programming language. Below is the course overview

## **Course Overview** ##

### About course :

*Learn to create Machine Learning Algorithms in Python and R from Data Science experts.*

### What we will learn :

* Master Machine Learning on Python & R

* Have a great intuition of many Machine Learning models

* Make accurate predictions

* Make powerful analysis

* Make robust Machine Learning models

* Create strong added value to your business

* Use Machine Learning for personal purpose

* Handle specific topics like Reinforcement Learning, NLP and Deep Learning

* Handle advanced techniques like Dimensionality Reduction

* Know which Machine Learning model to choose for each type of problem

* Build an army of powerful Machine Learning models and know how to combine them to solve any problem

### Languages Learned :

* Python

* R Programming Language

### IDE (Windows/Mac) :

* Spyder with Anaconda
* R Studio